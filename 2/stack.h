#ifndef STACK_H
#define STACK_H

/* a positive-integer value stack, with no size limit */
typedef struct stack
{
	int data;
	struct stack* next;
} stack;

void push(stack* s, unsigned int element);//add node to stack
int pop(stack* s); // Return -1 if stack is empty

void initStack(stack* s);//initialize stack
void cleanStack(stack* s);//free and delete stack

stack* newNode(int data);//create new node

#endif // STACK_H
